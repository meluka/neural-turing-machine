import torch
from torch import nn
import torch.nn.functional as F

class NTM(nn.Module):
    def __init__(self, num_inputs, num_outputs, controller, memory, heads):
        super(NTM, self).__init__()
        # save parameters
        self.num_inputs = num_inputs # number of inputs to the controller
        self.num_outputs = num_outputs # outputs of the final fc layer
        self.controller = controller # the controller object
        self.memory = memory # the memory object
        self.heads = heads # head object

        # size of the memory and controller
        self.N, self.M = memory.size()
        _, self.controller_size = controller.size()
        
        # initialize the first read with random numbers
        self.init_r = []
        num_read_heads = 0
        for head in heads:
            if head.is_read_head():
                init_r_bias = torch.randn(1, self.M) * 0.01 # initiate a read vector for each
                # read head
                num_read_heads += 1 # count the number of read heads
                self.init_r += [init_r_bias]
        
        # create the fc layer to output predictions
        self.fc = nn.Linear(self.controller_size + num_read_heads * self.M, self.num_outputs)
        # initiate param of the fc layer
        self.reset_parameters()

    def create_new_state(self, batch_size):
        # create new inital reads for a given batch size, clone from the init one
        init_r = [r.clone().repeat(batch_size, 1) for r in self.init_r]
        # create the initial controller state
        init_ctrl = self.controller.create_new_state(batch_size)
        # create the initial head state (weights for read and write through addressing method)
        init_heads = [head.create_new_state(batch_size) for head in self.heads]
        # return the initiated states as a pack (init_read, init_ctrl_state, init_head_state)
        return init_r, init_ctrl, init_heads

    def reset_parameters(self):
        # initiate the fc weights
        nn.init.xavier_uniform_(self.fc.weight, gain=1)
        # initiate the fc bias
        nn.init.normal_(self.fc.bias, std=0.01)

    def forward(self, x, prev_state):
        # unpack the (prev_read, prev_ctrl_state, prev_head_state)
        prev_read, prev_ctrl_state, prev_head_states = prev_state
        
        # cat the current input with prev_read
        input_1 = torch.cat([x] + prev_read, dim=1)
        #print(input_1.size())
        
        # compute the new controller output
        controller_output, controller_state = self.controller(input_1, prev_ctrl_state)

        head_state = []
        reads = []
        # compute the current head state (both read and write) and current reads
        for head, prev_head_state in zip(self.heads, prev_head_states):
            if head.is_read_head():
                r, w = head(controller_output, prev_head_state) # read and read_head_state
                reads += [r]
            else:
                w = head(controller_output, prev_head_state) # get the write_head_state

            # append the head states to a list
            head_state += [w]
        #print(type(r))
        # Compute the output from the final fc layer
        input_2 = torch.cat([controller_output] + reads, dim=1)
        o = F.sigmoid(self.fc(input_2))
        # pack the (current_read, current_ctrl_state, current_head_state)
        # return the output and packed states
        state = (reads, controller_state, head_state)
        return o, state

'''
# The dummy test
# define dummy variables
# for controller
num_inputs = 8
num_ctrl_inputs = 28
num_outputs = 8
d_bz = 1
# for ctrl
ctrl_size = 100
nlayers = 1
# for memory
N = 128
M = 20
# dummy inputs
d_x = torch.Tensor(1, 8)


from controller import LSTMController
d_ctrl = LSTMController(num_ctrl_inputs, ctrl_size, nlayers)

from memory import NTMMemory
d_memory = NTMMemory(N, M)
d_memory.reset(d_bz)

from head import NTMReadHead, NTMWriteHead
d_read_head = NTMReadHead(d_memory, ctrl_size)
d_write_head = NTMWriteHead(d_memory, ctrl_size)

d_heads = [d_read_head, d_write_head]

d_ntm =NTM(num_inputs, num_outputs, d_ctrl, d_memory, d_heads)

# try create_new_state function
prev_state = d_ntm.create_new_state(d_bz)
print(prev_state)

# try forward of ntm
o, state = d_ntm(d_x, prev_state)
print('ntm_output: {}'.format(o))
print('current_reads: {}'.format(state[0]))
print('current_ctrl_state: {}'.format(state[1]))
print('current_head_state: {}'.format(state[2]))
'''
