import torch
import torch.nn.functional as F
from torch import nn
import numpy as np

def _split_col(head_state, length):
    # split 2D matrix to variable length columns
    assert head_state.size()[1] == sum(length), 'lengths must be summed to num columns'
    # take the cumalative sum of the length vector to create the start and end index
    # pairs and loop to split the head_state in column-wise
    # the cumsumed pair vector looks like [0, 20, 21, 22, 25, 26]
    l = np.cumsum([0] + length)
    results = [] # store the splitted vectors
    # loop over and split the columns
    for s, e in zip(l[:-1], l[1:]):
        # append the columns
        results += [head_state[:, s:e]]
        
    return results

class NTMHeadBase(nn.Module):
    def __init__(self, memory, controller_size):
        # The head acts like the final layer of a fully
        # connected network. the output of the head is parsered
        # to get the coefficients and erase + add vectors for the addressing
        # mechanism.
        super(NTMHeadBase, self).__init__()
        # initiation
        self.memory = memory
        self.controller_size = controller_size
        self.N, self.M = memory.size()

        # dummy functions to inherit
    def create_new_state(self, batch_size):
        raise NotImplementError

    def register_parameters(self):
        raise NotImplementError

    def is_read_head(self):
        raise NotImplementError

    def _address_memory(self, k, beta, g, s, gamma, w_prev):
        # prepare the coeffs for addressing by using activation mechanisms
        k = k.clone()
        beta = F.softplus(beta)
        g = F.sigmoid(g)
        s = F.softmax(s, dim=1)
        gamma = 1 + F.softplus(gamma)
            
        # address the memory
        w = self.memory.address(k, beta, g, s, gamma, w_prev)

        return w

class NTMReadHead(NTMHeadBase):
    def __init__(self, memory, controller_size):
        super(NTMReadHead, self).__init__(memory, controller_size)

        # this corresponds the coeffs for read mechanism, as follows
        # k, beta, g, s, gamma
        self.read_lengths = [self.M, 1, 1, 3, 1]

        # create a FC layer to get the coeffs for reading from memory
        self.fc_read = nn.Linear(controller_size, sum(self.read_lengths))
        # initiate the fc_read layer
        self.reset_parameters()

    def create_new_state(self, batch_size):
        # The new state is created to hold the previous time steps' addressing
        # wights
        return torch.zeros(batch_size, self.N)

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.fc_read.weight, gain=1.4)
        nn.init.normal_(self.fc_read.bias, std=0.01)

    def is_read_head(self):
        return True

    def forward(self, x, w_prev):
        # The forward function of the read-head
        o = self.fc_read(x)
        # split the controller_state from the hidden layer of the
        # lstm controller and produce the coefficients
        # to calculate the weights for reading and writing of memory                
        k, beta, g, s, gamma = _split_col(o, self.read_lengths)
        
        w = self._address_memory(k, beta, g, s, gamma, w_prev)
        r = self.memory.read(w)
        
        return r, w
        
class NTMWriteHead(NTMHeadBase):
    def __init__(self, memory, controller_size):
        super(NTMWriteHead, self).__init__(memory, controller_size)

        # The write length vector, similar to read_lengths but with a and e
        self.write_length = [self.M, 1, 1, 3, 1, self.M, self.M]

        # create the FC layer to get the coeffs and add and erase to write to
        # memory
        self.fc_write = nn.Linear(self.controller_size, sum(self.write_length))
        # reset the parameters
        self.reset_parameters()

    def create_new_state(self, batch_size):
        return torch.zeros(batch_size, self.N)

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.fc_write.weight, gain=1.4)
        nn.init.normal_(self.fc_write.bias, std=0.01)

    def is_read_head(self):
        return False

    def forward(self, x, w_prev):
        # The forward function of write fc layer
        o = self.fc_write(x)
        # split the head_state to get the coefficients and add + erase
        k, beta, g, s, gamma, e, a = _split_col(o, self.write_length)
        # the erase vector should be between [0,1]
        e = F.sigmoid(e)
        # get the weights for writing by the addressing mechanism of the memory
        w = self._address_memory(k, beta, g, s, gamma, w_prev)
        # write to the memory
        self.memory.write(w, e, a)

        return w

'''
# dummy test of functionality

# dummy variables
d_N = 128
d_M = 20
d_bz = 1
d_ctrl_size = 100
d_w_prev = torch.Tensor(d_bz, d_N)
d_x = torch.Tensor(d_bz, d_ctrl_size)

# dummy memory
from memory import NTMMemory
d_memory = NTMMemory(d_N, d_M)
d_memory.reset(d_bz) # initiate the memory matrix

# dummy read head object
d_read_head = NTMReadHead(d_memory, d_ctrl_size)
# check dummy read heads functions
print('create_new_state: {}'.format(d_read_head.create_new_state(d_bz)))
r, w = d_read_head(d_x, d_w_prev)
#print('read vector: {}'.format(r))
#print('new weights: {}'.format(w))

# dummy write head object
d_write_head = NTMWriteHead(d_memory, d_ctrl_size)
# check dummy write heads functions
print('create_new_state: {}'.format(d_write_head.create_new_state(d_bz)))
w = d_write_head(d_x, d_w_prev)
print('new weights: {}'.format(w))
'''
