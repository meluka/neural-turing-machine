import torch
from torch.nn import Parameter
from torch import nn
import numpy as np

class LSTMController(nn.Module):
    def __init__(self, num_inputs, controller_size, num_layers):
        super(LSTMController, self).__init__()
        # create the controller and reset the parameters
        self.num_inputs = num_inputs
        self.controller_size = controller_size
        self.num_layers = num_layers

        # create the controller object
        self.lstm = nn.LSTM(input_size=self.num_inputs,
                            hidden_size=self.controller_size,
                            num_layers=self.num_layers)

        # initiate trainable parameters (lstm states) of the lstm controller.
        # The Parameter() is an iterator that initiates the lstm
        # states (hidden and cell states) with a given initiation method
        # the structure of states is [num_layers x batch_size x controller_size]
        # we take 1 as batch_size here since the complete initiation will be done
        # in the create_new_state function

        # hidden state initiation
        self.lstm_h_bias = Parameter(torch.randn(self.num_layers, 1, self.controller_size))
        # cell state initiation
        self.lstm_c_bias = Parameter(torch.randn(self.num_layers, 1, self.controller_size))

        # reset the lstm parameters
        self.reset_parameters()

    # initiate the lstm states
    def create_new_state(self, batch_size):
        lstm_h = self.lstm_h_bias.clone().repeat(1, batch_size, 1)
        lstm_c = self.lstm_c_bias.clone().repeat(1, batch_size, 1)

        return lstm_h, lstm_c

    # reset the lstm parameters
    def reset_parameters(self):
        # loop over the lstm parameters and check if its a bias or weight matrix
        for p in self.lstm.parameters():
            # if bias
            if p.dim() == 1:
                nn.init.constant_(p, 0)
            else:
                # if weights
                stdv = 5 / np.sqrt(self.num_inputs + self.controller_size)
                nn.init.uniform_(p, -stdv, stdv)

    # helper to get the input output dimensions of the lstm
    def size(self):
        return self.num_inputs, self.controller_size

    # forward function of the lstm
    def forward(self, x, prev_state):
        # compute the forward of lstm
        # in this method, we keep the lstm sequence length as 1,
        # since the structure of input is [sequence_length x batch_size x num_input_features]
        #print(x.size())
        
        x = x.unsqueeze(0)
        o, state = self.lstm(x, prev_state)

        return o.squeeze(0), state

'''
# dummy test of the controller
# dummy variables
#np_in = np.random.rand(1, 8)
d_in = torch.randn(1, 8)

d_in_size = 8
d_ctrl_size = 100
d_nlayers = 1
d_batch_size = 1
# dummy controller class
d_lstm = LSTMController(d_in_size, d_ctrl_size, d_nlayers)
# test functionality of size function
print(d_lstm.size())
# test functionality of create_new_state
h, c = d_lstm.create_new_state(d_batch_size)
print('hidden_state size: {}'.format(h.size()))
print('cell_state size: {}'.format(c.size()))
# test the functionality of forward
output, new_state = d_lstm(d_in, (h, c))
print('lstm_output: {}'.format(output))
print('lstm_new_state: {}'.format(new_state))
print('diff between h and c: {}'.format(new_state[0] - new_state[1]))
# since the last hidden state and the output are both equal following prints
# a matrix of zeros
print('diff between h and c: {}'.format(new_state[0] - output))
'''
