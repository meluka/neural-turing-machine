import torch
from torch import nn
from ntm import NTM
from controller import LSTMController
from head import NTMReadHead, NTMWriteHead
from memory import NTMMemory

class EncapsulatedNTM(nn.Module):
    def __init__(self, num_inputs, num_outputs, controller_size,
                 controller_layers, num_heads, N, M):
        super(EncapsulatedNTM, self).__init__()
        # save the variables
        self.num_inputs = num_inputs # number of ntm inputs
        self.num_outputs = num_outputs # number of ntm outputs
        self.controller_size = controller_size # controller size
        self.controller_layers = controller_layers # number of controller layers
        self.num_heads = num_heads # number of heads
        self.N = N # number of memory addresses
        self.M = M # number of memory features
        
        # initialize the corresponding objects (memory, heads, controller,
        # ntm)
        self.memory = NTMMemory(N, M) # initiate the memory
        controller = LSTMController(num_inputs + M * num_heads, controller_size,
                                    controller_layers) # initiate the controller
        heads = nn.ModuleList([])
        for i in range(num_heads):
            heads += [NTMReadHead(self.memory, controller_size),
                      NTMWriteHead(self.memory, controller_size)]

        self.ntm = NTM(num_inputs, num_outputs, controller, self.memory, heads)

    def init_sequence(self, batch_size):
        self.batch_size = batch_size
        # reset the memory
        self.memory.reset(batch_size)
        # create new ntm state
        self.prev_state = self.ntm.create_new_state(batch_size)

    def forward(self, x=None):
        if x is None:
            # this zero inputs are given to compute the outputs of the network
            # why? because ntm copy or repeated copy tasks work as follows,
            # you first give an input sequence, so the memory of the ntm will
            # remeber that sequence by embedding some features related to the
            # input sequence. once the ntm memory remebers the corresponding
            # features, you give a zero inputs, so the ntm should recover the
            # previous input sequence from the memory
            x = torch.zeros(self.batch_size, self.num_inputs)
        # forward through the network
        o, self.prev_state = self.ntm(x, self.prev_state)
        return o, self.prev_state

    def calculate_num_params(self):
        # return the number of parameters in the model
        num_parameters = 0
        for p in self.parameters():
            # vectorize the p and get the number of elements in the vector,
            # which is the number of parameters
            num_parameters += p.data.view(-1).size(0)

        return num_parameters
'''
# The dummy test
# dummy variables
num_inputs = 8
num_outputs = 8
d_bz = 1
N = 128
M = 20
num_heads = 1
ctrl_size = 100
nlayers = 1

# dummy input vector
x = torch.randn(1, 8)

# dummy encapsulated ntm object
d_enc_ntm = EncapsulateNTM(num_inputs, num_outputs, num_heads, ctrl_size, nlayers,
                           N, M)
# initialize the ntm
d_enc_ntm.init_seq(d_bz)

# call forward function
d_enc_ntm(x) # give the input sequence
out, state = d_enc_ntm() # remember the given sequence

print('ntm_input sequence: {}'.format(x))
print('ntm_output sequence: {}'.format(out))
'''    
