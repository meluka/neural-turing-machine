import torch
import torch.nn.functional as F
from torch import nn
import numpy as np

def _convolve1d(w_g, s):
    # s is the shift weighting that facilitates a head to shift the
    # focus to a different memory address, in order to access their
    # content. s is emmited by the controller and works in a modulo
    # fashion along the memory matrix. eg. shift forward at the bottom
    # of the memory focuses the heads attention to the top of the
    # memory matrix and vise-vesa. This is achieved by 1D convolution

    # pad (reflextion padding) the location based weight vector
    # print(t.size()) #--> num_address + 2
    t = torch.cat([w_g[-1:], w_g, w_g[:1]])
    # Do the convolutioal shift
    # print(c.size()) #--> num_address
    # the reshape done for t and s because of the pytorch standard for
    # 1D convolv.
    # input_shape - batch_size x input_channels x vector_len
    # weight_shape - output_channels x input_channels x vector_len
    c = F.conv1d(t.view(1, 1, -1), s.view(1, 1, -1)).view(-1)
    return c
    
    
class NTMMemory(nn.Module):
    def __init__(self, N, M):
        super(NTMMemory, self).__init__()
        # number of rows (mem addresses)
        self.N = N
        # number of cols (mem features)
        self.M = M

        # initiate the memory
        # the register_buffer of pytorch makes a stateful
        # part that is not a parameter but will be in the
        # state_dict
        self.register_buffer('mem_bias', torch.Tensor(N, M))

        # compute the std to initiate the memory matrix
        stdv = 1 / np.sqrt(N + M)
        nn.init.uniform_(self.mem_bias, -stdv, stdv)

    # reset memory
    def reset(self, batch_size):
        self.batch_size = batch_size
        # initialize the memory for a batch size
        self.memory = self.mem_bias.clone().repeat(batch_size, 1, 1) # the gradients
        # computed on cloned tensor will be
        # propegated to the original (mem_bias)
        # tensor
    
    # memory size
    def size(self):
        return self.N, self.M
    
    # read from memory
    def read(self, w):
        # Implement the read algorithm
        # r_t = _sum_[i](w_t(i)*M_t(i))
        # t_th instanse and i_th address
        # the read output is (batch_size x 1 x mem_features)
        # but returned read output is (batch_size x mem_features))
        return torch.matmul(w.unsqueeze(1), self.memory).squeeze(1)
    
    # write to memory
    def write(self, w, e, a):
        # e and a are batch_size x mem_features
        # Implement the write algorithm
        # first erase from memory and the add to memory
        # M_t = M_t-1[1 - w_t*e_t] ; e_t erase vector emmited by controller
        # M_t = M_t + wt*a_t ; a_t add vector emmited by controller

        self.prev_mem = self.memory
        self.memory = torch.Tensor(self.batch_size, self.N, self.M)

        #self.memory = torch.Tensor(self.batch_size, self.N, self.M)
        #self.memory = self.prev_mem * (1 - torch.matmul(w.unsqueeze(-1),
        #                                                e.unsqueeze(1)))
        #self.memory = self.memory + torch.matmul(w.unsqueeze(-1),
        #                                         a.unsqueeze(1))

        erase = torch.matmul(w.unsqueeze(-1), e.unsqueeze(1))
        add = torch.matmul(w.unsqueeze(-1), a.unsqueeze(1))
        self.memory = self.prev_mem * (1 - erase) + add
        
    # addressing
    def address(self, k, beta, g, s, gamma, w_prev):
        # Implement the addressing mechanism
        # the addressing pipeline as follows
        # 1st do content based addressing
        # then do the location based addressing with conv shif and sharpning

        # k: the key vector (batch_size x mem_features)
        # beta: the key strength (focus)
        # g: scalar interpollation gate (with w_prev)
        # s: shift weighting
        # gamma: sharpen weighting
        # w_prev: pervious time step weights

        # reshape k from batch_size x mem_features to
        # batch_size x 1 x mem_features
        #k = k.unsqueeze(1)
        k = k.view(self.batch_size, 1, -1)
        # --Content based addressing-- #
        # the cosine_similarity between memory features and k is been checked
        # using F.cosine_similarity (dim=-1 is the mem_features)
        # then the softmax each simalrity is computed (dim=1 is along the
        # memory addresses)

        # print(w_c.size()) #--> batch_size x num_address
        # print(k.size()) #--> batch_size x mem_features
        w_c = F.softmax(beta * F.cosine_similarity(self.memory + 1e-16,
                                                   k + 1e-16, dim=-1), dim=1)
        #print(k)
        # --Location based addressing-- #
        # print(w_g.size()) #--> batch_size x num_address
        # print(g.size()) #--> batch_size x 1
        w_g = g * w_c + (1 - g) * w_prev
        
        # --convolutioal shift in general form-- #
        # print(s.size()) #--> batch_size x 3
        # print(w_tild.size()) #--> batch_size x num_address
        #w_tild = torch.zeros(self.batch_size, self.N)
        w_tild = torch.zeros(w_g.size())
        for b in range(self.batch_size):
            w_tild[b] = _convolve1d(w_g[b], s[b])

        # --weight sharpning-- #
        # print(w_hat.size()) #--> batch_size x num_address
        # print(gamma.size()) #--> 1 x 1
        # print(w.size()) #--> batch_size x num_address
        # the view is done because, torch.sum returns a scalar in this case, with
        # dim=1, but for division with w_tild we need dim=2. Therefor the view
        # results a scalar with shape of 1 x 1
        w = torch.div(w_tild ** gamma, torch.sum(w_tild ** gamma, dim=1).view(-1, 1) + 1e-16)

        return w
        
'''
# Dummy test of the class and functions
num_address = 128
mem_features = 20
# dummy weight vector
w = torch.Tensor(1, num_address)
# dummy erase and add vectors
e = torch.Tensor(1, mem_features)
a = torch.Tensor(1, mem_features)
# dummy controller output coeffs
k = torch.Tensor(1, mem_features)
beta = torch.Tensor(1, 1)
g = torch.Tensor(1, 1)
s = torch.Tensor(1, 3)
gamma = torch.Tensor(1, 1)

d_class_memory = NTMMemory(num_address, mem_features)
# check size
print('mem_size: {}'.format(d_class_memory.size()))
# check reset functionality
d_class_memory.reset(1)
print('memory_mat: {}'.format(d_class_memory.memory))
# test read functionality
r = d_class_memory.read(w)
print('read_vect: {}'.format(r))
# test the write functionality
current_mem = d_class_memory.memory
# do write
d_class_memory.write(w, e, a)
new_mem = d_class_memory.memory
print('mem_difference: {}'.format(new_mem - current_mem))
# test the addressing functionality
new_w = d_class_memory.address(w, k, beta, g, s, gamma)
print('weight_difference: {}'.format(w - new_w))
'''
