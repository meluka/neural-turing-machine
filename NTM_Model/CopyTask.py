from aio import EncapsulatedNTM

from attr import attrs, attrib, Factory
import random

import torch
from torch import nn, optim
import numpy as np

# This function is a generator, not a iterator like a list or string
# That means, you can call this function onces,the second call will create
# a new generator object. the created gernator object will then yield the
# values from the for loop. Note that you cant use return keyword since
# you are accessing a generator rather than a iterator
def dataloader(num_batches, batch_size, seq_width, min_len, max_len):
    # generate random bit sequance for the copy task
    # the sequance has random lengths, but always between
    # min_len and max_len. The last column and last row of a given
    # sequance is a delimiter indicating the end of the sequance.
    for batch in range(num_batches):
        # radomly select the sequance lengths
        seq_len = random.randint(min_len, max_len)
        # create a matrix of random bits for the seq_len and seq_width
        seq = np.random.binomial(1, 0.5, size=(seq_len, batch_size, seq_width))
        # convert it to a torch tensor
        seq = torch.from_numpy(seq)

        # introduce the delimiter to the sequance
        inputs = torch.zeros(seq_len+1, batch_size, seq_width+1)
        # insert the generated random bit sequance
        inputs[:seq_len, :, :seq_width] = seq
        # set the delimiter to 1
        inputs[seq_len, :, seq_width] = 1

        # clone the inputs to outputs variable (copy task)
        outputs = inputs.clone()
        # yield the sequance
        # 
        yield batch + 1, inputs.float(), outputs.float()


'''
# dummy test of the dataloader
loader = dataloader(3, 1, 8, 3, 5)
for i, x, y in loader:
    print('batch: {}'.format(i))
    print('input: {}'.format(x))
    print('output: {}'.format(y))

# do a dummy training to test the model
# dummy variables for model
d_input_size = 8 # seq_len = 8 + delimiter
d_output_size = 8 # seq_len = 8 + delimiter
d_N = 128
d_M = 20
d_num_heads = 1
d_ctrl_size = 100
d_nlayers = 1

# dummy training variables
d_num_batches = 100000
d_bz = 1
d_lr = 1e-4
d_mmnt = 0.9
d_alp = 0.95

# the dummy model object
d_ntm = EncapsulatedNTM(d_input_size+1, d_output_size+1, d_ctrl_size,
                        d_nlayers, d_num_heads, d_N, d_M)

optimizer = optim.RMSprop(d_ntm.parameters(), lr=d_lr, momentum=d_mmnt, alpha=d_alp)
#optimizer = optim.SGD(d_ntm.parameters(), lr=d_lr, momentum=d_mmnt)
criterion = nn.BCELoss() 
dummy_loader = dataloader(d_num_batches, d_bz, d_input_size, 1, 20) # make the dummy generator object

# set random seeds
RANDOM_SEED = 1000
np.random.seed(RANDOM_SEED)
torch.manual_seed(RANDOM_SEED)
random.seed(RANDOM_SEED)

# gradient clipping function
def clip_grads(model):
    # clip the gradients to range [-10, 10]
    param = list(filter(lambda p: p.grad is not None, model.parameters()))
    for p in param:
        #print('clipping')
        p.grad.data.clamp_(-10, 10)

costs = []
for batch, x, y in dummy_loader:
    optimizer.zero_grad()
    inp_seq_len = x.size(0)
    out_seq_len, batch_size, _ = y.size()
    #print(inp_seq_len)
    #print(out_seq_len)

    # Initiate the ntm network
    d_ntm.init_sequence(batch_size)
    
    # give the sequence to the network
    for i in range(inp_seq_len):
        d_ntm(x[i])

    # Now give no inputs (at ntm encapsulation, if x=None, it gives
    # a input of zeros, so only the last fc of ntm works with the
    # previous reads (we are trying to remember an input right?, so
    # once the memory is updated from the previous step (for loop above),
    # now the task is to remember the pattern from the memory))
    y_out = torch.zeros(y.size())
    for i in range(out_seq_len):
        y_out[i], _ = d_ntm()

    # Compute the loss
    loss = criterion(y_out, y)
    # Backpropagate the error
    loss.backward()
    # clip the gradients
    clip_grads(d_ntm)
    # Update the weights
    optimizer.step()

    # binarize the outputs
    y_out_binarized = y_out.clone().data
    # print(y_out_binarized)
    y_out_binarized.apply_(lambda x: 0 if x < 0.5 else 1)
    #print('element_mean: {}'.format(torch.mean(y_out_binarized - y)))
    #print('y_out: {}'.format(y_out_binarized))
    #print('Y: {}'.format(y.data))
    #cost = np.abs(torch.mean(y_out.data - y.data).item())
    cost = torch.sum(torch.abs(y_out_binarized - y.data))

    costs += [cost.item()]
    # print(np.array(costs).mean())
    if batch % 10 == 0:
        print('batch: {}, cost: {}'.format(batch, np.array(costs).mean()))
'''
