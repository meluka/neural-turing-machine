This code base is an attempt to reverse engineer a Neural Turing Machine code.
The original code can be found in the following link: https://github.com/loudinthecloud/pytorch-ntm.git
The class diagram of the original work can be found in the class_ntm.png image.
The class diagram was reverse engineered using Pyreverse library (link: https://www.logilab.org/blogentry/6883)
TODO: Complete the copy task.
TODO: Extend the task list with Repeated Copy task.